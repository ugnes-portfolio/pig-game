Desktop version only

# Learning
- Implement dice roll logic
- Switch active player logic
- Hold the score logic

# How to use
- The goal is to be the first to reach 100 points.
- Click "Roll dice". Every time you roll the dice, the dice image updates, and the current dice roll is added to your current score. If you are satisfied with your rolls, click "hold" to keep the scores. They will be added to your final score. If you roll a 1, your current scores are lost, and it becomes the other player's turn.
- When a player reaches 100 points or more, background color changes, the dice cannot be rolled.
- You can press "New game" and start again
